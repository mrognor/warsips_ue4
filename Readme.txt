Сборка:

1. Необходима Visual Studio 2019
2. Скачать исходный код Unreal Engine 4.26.2:
https://docs.unrealengine.com/4.26/en-US/ProgrammingAndScripting/ProgrammingWithCPP/DownloadingSourceCode/, 
следовать инструкции:
https://docs.unrealengine.com/4.26/en-US/ProductionPipelines/DevelopmentSetup/BuildingUnrealEngine/
вплоть до сборки. Перед сборкой найти файл BuoyancyComponent.h и заменить на изменённый:
https://disk.yandex.ru/d/v6gS5uxjyFDV6g
3. Собрать Unreal Engine

Открытие проекта:

1. Запустить Warships.uproject. Он предложит сгенерировать модуль. Подтвердить всё.
2. Нажать правой кнопкой мыши на Warships.uproject и в контекстном меню нажать на 
Generate Visual Studio project files
3. Открыть .sln файл и собрать проект

Далее запускать как обычный проект

Починка симуляции волн (епики забыли положить материал в нужную папку):

1. В движке в content browser во view options выбрать "Show plugin content" 
2. Перейти в \Water Content\Fluid Simulation\Materials\Simulation
3. Открыть материал M_Fluid_Sim_01
4. Сверху, в области foam sim forces найти текстуру, мешающую сборке (подсвечена) и выделить
5. Во вкладке Details в параметре Texture найти и выбрать текстуру с названием FoamRT
6. Сохранить материал
7. Перезапустить проект

